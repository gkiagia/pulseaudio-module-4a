/***
  This file is part of PulseAudio.

  Copyright 2018 Collabora Ltd.
    Author: George Kiagiadakis <george.kiagiadakis@collabora.com>

  PulseAudio is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published
  by the Free Software Foundation; either version 2.1 of the License,
  or (at your option) any later version.

  PulseAudio is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with PulseAudio; if not, see <http://www.gnu.org/licenses/>.
***/

#ifndef __M4A_AFB_COMM_H__
#define __M4A_AFB_COMM_H__

#include <stdbool.h>
#include <json-c/json.h>

typedef struct _m4a_afb_comm m4a_afb_comm;

enum m4a_afb_reply {
    M4A_AFB_REPLY_OK,
    M4A_AFB_REPLY_ERROR
};

typedef void (*m4a_afb_done_cb_t)(enum m4a_afb_reply r,
                                  json_object *response,
                                  void *userdata);

bool m4a_afb_call_async(m4a_afb_comm *comm,
                        const char *verb,
                        json_object *object,
                        m4a_afb_done_cb_t done_cb,
                        void *userdata);

m4a_afb_comm *m4a_afb_comm_new(const char *uri);
void m4a_afb_comm_free(m4a_afb_comm *comm);

#endif
